# go-common

Common Go stuff, or at least things that I tend to use in multiple places.  
The purpose of using an Unlicense is because, rather than using this repo as a module, I would encourage you to instead copy/paste if needed rather than bloat your code with a bunch of utilites that you might not use.  
This repo will still use semver in case you prefer a module, though. 🙂

Feel free to PR if you want to add something, just keep in mind the licensing means anyone else can use it without attribution.

## License

[Unlicense](LICENSE)