package epoch

import (
	"testing"
	"time"
)

const secs = 1616025600

func TestEpoch(t *testing.T) {
	e := Epoch(secs)
	u := time.Unix(secs, 0)
	if e.Time() != u {
		t.Log("epoch did not match time with matching seconds")
		t.FailNow()
	}
}

func TestEpochZero(t *testing.T) {
	if !Zero.Time().IsZero() {
		t.Log("epoch did not match zero-time")
		t.FailNow()
	}
}
