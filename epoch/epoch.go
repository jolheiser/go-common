package epoch

import "time"

// Zero passes time.Time.IsZero
const Zero = Epoch(-62135596800)

// Epoch is a unix timestamp (seconds)
//
// Any time-specific calculations should be done with Epoch.Time
type Epoch int64

// Time returns the corresponding time.Time from this Epoch
func (e Epoch) Time() time.Time {
	return time.Unix(int64(e), 0)
}

// Now returns the current Unix Epoch
func Now() Epoch {
	return Epoch(time.Now().Unix())
}
